const {copyDirectory, sass, js} = require('laravel-mix');
const mix = require('laravel-mix');
const LiveReloadPlugin = require('webpack-livereload-plugin');

sass('app/assets/sass/style.scss', 'app/assets/css/styles.css')
//sass('app/assets/fonts/icomoon/sass/style.scss', 'app/assets/css/font.css')

mix.webpackConfig({
    plugins: [
        new LiveReloadPlugin()
    ]
});