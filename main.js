const { app, BrowserWindow, ipcMain } = require('electron');
const log = require('electron-log');
const {autoUpdater} = require('electron-updater');
const path = require('path');
const notifier = require('node-notifier')
const {is} = require('electron-util');
const Store = require('electron-store');
const Toaster = require('electron-toaster');

const Storage = require(path.join(__dirname, 'app/assets/scripts/Utils/LocalStorage.js'))
require('dotenv').config({ path: './app/.env' });
console.log(app.getPath('userData'))
if(is.development === true) {
    let store = new Store();
    store.clear();
}

//
const debug = /--debug/.test(process.argv[2]);
let storage = new Storage();
let toaster = new Toaster();
//
app.disableHardwareAcceleration();
app.setAppUserModelId(process.execPath);


if (process.mas) app.setName('Interlocking Launcher');

autoUpdater.logger = log;
autoUpdater.logger.transports.file.level = 'info';
log.info('App starting...');

let win;
let loadingScreen;
let template = [];
let notification;
app.commandLine.appendSwitch('ignore-certificate-errors');

function createWindow() {
    win = new BrowserWindow({
        width: 1250,
        height: 830,
        icon: getPlatformIcon('trainznation'),
        frame: false,
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
            enableRemoteModule: true,
            worldSafeExecuteJavaScript: true,
            webSecurity: false
        },
        backgroundColor: '#171614',
        show: false
    });
    win.on('close', () => {
        win = null;
    });
    win.removeMenu();
    win.resizable = true;

    if(is.development === true) {
        win.webContents.openDevTools();
        win.maximize();
    }

    win.loadURL(`file://${__dirname}/app/html/index.html`).then(r => {
        return win;
    });


    win.once('ready-to-show', () => {
        autoUpdater.checkForUpdatesAndNotify();
    });

    win.webContents.on('did-finish-load', () => {
        if(loadingScreen) {
            loadingScreen.close()
        }

        //win.show();
    })

    return win;
}

function createLoadingScreen() {
    loadingScreen = new BrowserWindow(
        Object.assign({
            width:400,
            height: 400,
            frame: false,
            transparent: true,
            webPreferences: {
                nodeIntegration: true,
                contextIsolation: false,
                enableRemoteModule: true,
                worldSafeExecuteJavaScript: true,
                webSecurity: false
            },
        })
    );

    loadingScreen.setResizable(false)
    loadingScreen.loadURL(`file://${__dirname}/app/html/loading.html`).then(r => {
        return loadingScreen;
    });
    loadingScreen.on('close', () => {
        loadingScreen = null
    });

    if(is.development === true) {
        loadingScreen.webContents.openDevTools();
        loadingScreen.maximize();
    }

    loadingScreen.webContents.on('did-finish-load', () => {
        loadingScreen.show();
    })
}

function getPlatformIcon(filename){
    const opSys = process.platform;
    if (opSys === 'darwin') {
        filename = filename + '.icns'
    } else if (opSys === 'win32') {
        filename = filename + '.ico'
    } else {
        filename = filename + '.png'
    }

    return path.join(__dirname, 'app', 'assets', 'images', filename)
}


app.on('ready', () => {
    createLoadingScreen()
    //createWindow();
    /*setTimeout(() => {
        createWindow()
    }, 10000)*/
});

app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
        createWindow()
    }
});

ipcMain.on('app_version', (event) => {
    event.sender.send('app_version', { version: app.getVersion() });
});

ipcMain.on('request-asset-data', (event, data) => {
    event.sender.send('action-asset-data', data)
})

ipcMain.on('loading-terminate', (event, arg) => {
    if(arg === 'ok') {
        createWindow()
    } else {

    }
})

autoUpdater.on('update-available', () => {
    log.info("Mise à jour disponible")
    win.webContents.send('update_available');
});
autoUpdater.on('update-downloaded', () => {
    log.info('Mise à jour télécharger')
    win.webContents.send('update_downloaded');
});

