const ps3 = new PerfectScrollbar(document.querySelector('#routeContainer'))
const ps4 = new PerfectScrollbar(document.querySelector('#roadmap'), {
    suppressScrollY: true
})
const moment = require('moment')
const notif = require('electron-toaster')
const onezip = require('onezip')

ps3.update();
ps4.update();

const now = moment().format('YYYY-MM-DD hh:mm:ss')

let ElementsRoute = {
    routeContainer: utils.getId('routeContainer'),
    loadingRoad: utils.getId('loadingRoad'),
    collapseHead: document.querySelectorAll('.collapse-head')
}


Array.from(ElementsRoute.collapseHead).forEach((elem) => {
    const ps5 = new PerfectScrollbar(elem)
    ps5.update()
})

toastr.info('Testing')

function block() {
    ElementsRoute.loadingRoad.innerHTML = `
    <div class="d-flex justify-content-center">
        <div class="spinner-border text-primary" style="width: 5rem; height: 5rem;" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
    `
}

function unblock() {
    ElementsRoute.loadingRoad.style.display = 'none'
}

// Fonction chargement des routes

function loadListRoute() {
    block()

    $.ajax({
        url: `${store.get('API_ENDPOINT')}launcher/route/list`,
        success: (data) => {
            unblock()
            Array.from(data.data).forEach((item) => {

                let installedField;
                db.getRows('routes', {
                    identifier: item.id,
                }, (succ, result) => {
                    if (succ) {
                        installedField = (result[0].installed === true) ? 1 : 0
                    }
                })

                let installed = {
                    0: {"text": "Non installer", "class": "danger"},
                    1: {"text": "Installer", "class": "danger"},
                    2: {"text": "Mettre à jour", "class": "warning"},
                }
                ElementsRoute.routeContainer.innerHTML += `
                <div class="col-md-4">
                    <div class="card bg-dark animate text-white">
                        <img src="${item.image}" class="card-img" alt="...">
                        
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-9"><h3 class="card-title">${item.name}</h3></div>
                                <div class="col-md-3">
                                    <span class="h3 badge badge-success">${'V' + item.version}:${item.build}</span>
                                    <span class="badge badge-${installed[installedField].class}">${installed[installedField].text}</span>
                                </div>
                            </div>
                            <p>${item.description}</p>
                        </div>
                        <div class="card-footer text-center">
                            <button class="btn btn-link btn-route-action" data-id="${item.id}">En savoir plus</button>
                        </div>
                    </div>
                </div>
                `
            })
            viewRoute();
        },
        error: error => {
            notify.errorRestart(error.responseText)
        }
    })
}

function openModalRoute(evt, route_id) {
    let modal = $("#modalRoute");
    modal.modal('show')
    /*modal.find('.modal_contenue').html(`<div class="d-flex justify-content-center">
          <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
            <span class="sr-only">Loading...</span>
          </div>
        </div>`)*/

    $.ajax({
        url: store.get("API_ENDPOINT") + 'launcher/route/' + route_id,
        success: (data) => {
            db.getRows('routes', {"identifier": data.data.id}, (succ, result) => {
                affichageDetail(modal, data.data, result[0]);
                afficheSessions(modal, data.data)
                afficheRoadmap(modal, data.data)
            })
            document.getElementById('progress-body').style.display = 'none'
            downloadRoute(data.data.id, data.data.version, data.data.build)
        }
    })
}

function editImages(modal, data) {

}

function viewRoute() {
    let btns = document.querySelectorAll('.btn-route-action')
    Array.from(btns).forEach((btn) => {
        btn.addEventListener('click', () => {
            openModalRoute(event, btn.dataset.id)
        })
    })
}

function affichageDetail(modal, data, database) {
    let elem = modal.find('#details')

    function verifTextVersion() {
        if (database.installed === 1) {
            if (database.installed_version !== database.serveur_version) {
                return `<strong>Version Installé:</strong> ${database.installed_version} | <span class="text-danger font-weight-bold">Version Serveur: ${database.serveur_version}</span>`
            } else {
                return `<strong>Version Installé:</strong> ${database.installed_version} | <strong>Version Serveur:</span> ${database.serveur_version}`
            }
        } else {
            return `<strong>Version Serveur:</strong> ${database.serveur_version}`
        }
    }

    function verifButtonVersion() {
        if (database.installed === 1) {
            if (database.installed_version !== database.serveur_version) {
                return `<button class="btn btn-warning btn-route-upload" data-id="${data.id}"><i class="icon-update"></i> Mettre à jour</button>`
            } else {

            }
        } else {
            return `<button class="btn btn-success btn-route-download" data-id="${data.id}"><i class="icon-download"></i> Télécharger</button>`
        }
    }

    elem.html(`
    <div class="container mb-2 mt-2">
        <div class="card bg-dark">
            <div class="card-body">
                <h1 class="text-center">${data.name}</h1>
            </div>
        </div>
    </div>
    <img src="${data.image}" class="img-fluid" alt="">
    <div class="asset-action-bar">
        <div class="row">
            <div class="col-md-5">
                ${verifTextVersion()}
            </div>
            <div class="col-md-3">
                <span class="text-center download-text"></span>
            </div>
            <div class="col-md-4 text-right">
                ${verifButtonVersion()}
            </div>
        </div>
    </div>
    <div id="progress-body">
        <div class="progress" style="height: 5px;">
            <div class="progress-bar bg-info" role="progressbar" style="width: 0;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
    </div>
    <div class="container">
        <div class="card text-dark mt-4 mb-4">
            <div class="card-body">
                ${data.description}
            </div>
        </div>
    </div>
    `)
}

function afficheSessions(modal, data) {
    let elem = modal.find('#session')

    elem.html(`
    <div class="container text-dark">
        <h1 class="text-center text-white">Liste des sessions</h1>
        <div id="listeSession"></div>
    </div>
    `)

    function chargeElement() {
        Array.from(data.sessions).forEach((session) => {

            let database = null;
            db.getRows('sessions', {identifier: session.id}, (succ, result) => {
                database = result[0]
            })

            function verifPublished() {
                if (session.published_at.normalize > moment().format('YYYY/MM/DD HH:ii:ss')) {
                    return `<div class="ribbon ribbon-warning float-left"><i class="icon-info mr-1"></i> Bientôt disponible <span class="dot"></span> <small>(Disponible ${session.published_at.human})</small> </div>`
                } else {
                    return `<div class="ribbon ribbon-success float-left"><i class="icon-check mr-1"></i> Disponible <span class="dot"></span> <small>(Disponible ${session.published_at.human})</small></div>`
                }
            }

            function buttonElement() {
                if (database.installed === 1) {
                    if (database.installed_at < database.updated_at) {
                        return `<button class="btn btn-warning btn-upload-session" data-id="${session.id}"><i class="icon-update"></i> Mettre à jour</button>`
                    } else {

                    }
                } else {
                    return `<button class="btn btn-success btn-download-session" data-id="${session.id}"><i class="icon-download"></i> Télécharger</button>`
                }
            }

            document.getElementById('listeSession').innerHTML += `
            <div class="card mb-3 ribbon-box">
                ${verifPublished()}
                <div class="row no-gutters ribbon-content">
                    <div class="col-md-4">
                        <img src="${session.image}" class="card-img" alt="...">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">${session.name}</h5>
                            <p class="card-text">${session.short_content}</p>
                        </div>
                        <div class="card-footer">
                            <div class="row no-gutters">
                                <div class="col-md-6"></div>
                                <div class="col-md-6 text-right">
                                    ${buttonElement()}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            `
        })
    }

    chargeElement()
}

function afficheRoadmap(modal, data) {
    let elem = modal.find('#roadmap')
    elem.html(`
    <div class="container-fluid mt-2 mb-2">
        <div class="row" id="listeRoadmap">
            ${loadVersion()}
        </div>
    </div>
    `)

    function loadVersion() {
        let divVersion = '<span></span>';
        Array.from(data.roadmaps.versions).forEach((version) => {

            function showPublishedAt() {
                if (version.published_at.normalize > now) {
                    return `<small>Publier le ${version.published_at.serialize}</small>`
                } else {
                    return `<small>Publier ${version.published_at.human}</small>`
                }
            }

            divVersion += `
            <div class="col-md-4">
                <div class="card card-roadmap">
                    <header class="card-roadmap-header">
                        <h3 class="card-roadmap-title">V${version.version}</h3>
                        ${showPublishedAt()}
                    </header>
                    <div class="card-roadmap-body">
                        <div class="accordion" id="accordionExample">
                            ${loadCategories(version)}
                        </div>
                    </div>
                </div>
            </div>
            `
        })

        return divVersion;
    }

    function loadCategories(version) {
        let divCategory = '<span></span>'
        Array.from(version.categories).forEach((category) => {
            divCategory += `
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#${category.slug}" aria-expanded="true" aria-controls="${category.slug}">
                            ${category.name} <br>
                            <span style="font-size: 15px;">${category.total_task} Entrées</span> <span class="dot"></span> <span style="font-size: 10px; color: darkgrey">${category.waiting_task} en développement</span>
                        </button>
                    </h2>
                </div>

                <div id="${category.slug}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body" id="listeCard">
                        ${loadCard(category)}
                    </div>
                </div>
            </div>
            `
        })

        return divCategory;
    }

    function loadCard(category) {
        let divCard = '<span></span>'
        Array.from(category.cards).forEach((card) => {

            function progressing() {
                if (Object.keys(card.tasks.count).length !== 0) {
                    if (card.tasks.count.total_task !== card.tasks.count.finish) {
                        return `
                        <div class="card_progress_wrapper">
                            <div class="progress_wrapper">
                                <p class="progress_textual_progress">
                                    <span class="progress_stat">${card.tasks.count.finish}/${card.tasks.count.total_task}</span> taches accomplies
                                </p>
                                <div class="progress_progress_bar">
                                    <div class="progress_completion" style="width: ${card.tasks.count.percent_dev}%"></div>
                                </div>
                            </div>
                        </div>
                        `
                    } else {
                        return '&nbsp;';
                    }
                } else {
                    return '&nbsp;';
                }
            }

            divCard += `
            <div class="card_wrapper">
                <div class="card_zone">
                    <a href="/">
                        <header>
                            <h3 class="card_title">${card.name}</h3>
                            <svg viewBox="0 0 10 10" class="card_icon_viewbox"><polygon points="247.333 13.845 247.333 17.667 249 17.667 249 11 242.333 11 242.333 12.667 246.155 12.667 240.667 18.155 240.667 14.333 239 14.333 239 21 245.667 21 245.667 19.333 241.845 19.333" transform="translate(-239 -11)"></polygon></svg>
                        </header>
                        <figure media="https://via.placeholder.com/260x163" class="card_thumbnail" style="background-image: url('https://via.placeholder.com/260x163')"></figure>
                    </a>
                    ${progressing()}
                    <section class="card_text">
                        <span class="card_status_label">Etat:</span>
                        <span class="card_status">${card.status}</span>
                        <p class="card_description">${card.description}</p>
                    </section>
                </div>
            </div>
            `
        })

        return divCard;
    }
}

function downloadRoute(route_id, version, build) {
    let btns = document.querySelectorAll('.btn-route-download')

    Array.from(btns).forEach((btn) => {
        btn.addEventListener('click', (event) => {
            let startTime = new Date();
            process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;
            const uri = store.get('DOWN_ENDPOINT') + 'route/' + route_id + '/route_' + version + '_' + build + '.zip';
            const dl = new DownloaderHelper(uri, app.getPath('userData') + '/routes/' + route_id + '/', {
                override: true
            })

            let progressBody = document.querySelector('#progress-body');
            let downloadText = document.querySelector('.download-text')
            let progressBar = $(".progress-bar")

            dl.start()
            dl
                .on('download', downloadInfo => {
                    btn.innerHTML = '<i class="fa fa-spinner fa-spin"></i>'
                    btn.setAttribute('disabled', 'true')
                    downloadText.innerHTML = 'Téléchargement en cours...'

                    progressBody.style.display = 'block'
                    progressBar
                        .attr('style', 'width: 0;')
                        .attr('aria-valuenow', 0)
                        .removeClass('bg-info')
                })

                .on('end', downloadInfo => {
                    btn.innerHTML = '<i class="icon-download"></i> Télécharger'
                    btn.removeAttribute('disabled')

                    progressBody.style.display = 'none'

                    downloadText.innerHTML = '<i class="icon-check"></i> Téléchargement terminer'

                    setTimeout(() => {
                        extractRoute(route_id, version, build)
                    }, 1200)
                })

                .on('skip', skipInfo => {
                    btn.innerHTML = '<i class="icon-download"></i> Télécharger'
                    btn.removeAttribute('disabled')

                    progressBody.style.display = 'none'

                    downloadText.innerHTML = '<i class="icon-arrow-left"></i> Téléchargement Interrompu'

                    let msg = {
                        title: 'Téléchargement stoppée',
                        message: `Le fichier ${skipInfo.fileName} existe déja !`,
                        width: 400,
                        timeout: 6000,
                        focus: true // set focus back to main window
                    }
                    ipc.send('electron-toaster-message', msg)
                })
                .on('error', err => {
                    btn.innerHTML = '<i class="icon-download"></i> Télécharger'
                    btn.removeAttribute('disabled')

                    progressBody.style.display = 'none'

                    downloadText.innerHTML = '<i class="icon-warning"></i> Erreur de téléchargement'

                    let msg = {
                        title: 'Erreur de téléchargement',
                        message: `Erreur: ${err}`,
                        width: 400,
                        timeout: 6000,
                        focus: true // set focus back to main window
                    }
                    ipc.send('electron-toaster-message', msg)
                })
                .on('retry', (attempt, opts) => {

                })
                .on('resume', isResumed => {
                    // is resume is not supported,
                    // a new pipe instance needs to be attached
                    if (!isResumed) {
                        dl.unpipe();
                        console.warn("This URL doesn't support resume, it will start from the beginning");
                    }
                })
                .on('stateChanged', state => null)
                .on('renamed', filePaths => null)
                .on('progress', stats => {
                    const progress = stats.progress.toFixed(1);
                    const speed = byteHelper(stats.speed);
                    const downloaded = byteHelper(stats.downloaded);
                    const total = byteHelper(stats.total);

                    // print every one second (`progress.throttled` can be used instead)


                    const currentTime = new Date();
                    const elaspsedTime = currentTime - startTime;
                    if (elaspsedTime > 100) {
                        progressBar
                            .attr('style', 'width: ' + progress + '%;')
                            .attr('aria-valuenow', progress)

                        startTime = currentTime;
                        console.log(`${speed}/s - ${progress}% [${downloaded}/${total}]`);
                        downloadText.innerHTML = `${downloaded} sur ${total} à ${speed}/s`
                        if (progress <= 33) {
                            progressBar.addClass('bg-danger')
                        } else if (progress >= 34 && progress <= 66) {
                            progressBar.removeClass('bg-danger')
                            progressBar.addClass('bg-warning')
                        } else {
                            progressBar.removeClass('bg-warning')
                            progressBar.addClass('bg-success')
                        }
                    }
                });

            console.log('Downloading: ', uri);
        })
    })
}

function extractRoute(route_id, version, build) {

    let progressBody = document.querySelector('#progress-body');
    let downloadText = document.querySelector('.download-text')
    let progressBar = $(".progress-bar")
    let nameZip = `route_${version}_${build}.zip`
    let nameRoute = `route_${version}_${build}`
    let startTime = new Date();

    const extract = onezip.extract(`${app.getPath('userData')}/routes/${route_id}/route_${version}_${build}.zip`, `${app.getPath('userData')}/routes/${route_id}/tmp`)

    extract.on('start', () => {
        downloadText.innerHTML = '<i class="fa fa-spinner fa-spin"></i> Extraction de la route en cours...'

        progressBody.style.display = 'block'
        progressBar
            .attr('style', 'width: 0;')
            .attr('aria-valuenow', 0)
            .removeClass('bg-info')
    })

    extract.on('progress', (percent) => {
        const currentTime = new Date();
        const elaspsedTime = currentTime - startTime;

        if (elaspsedTime > 100) {
            progressBar
                .attr('style', 'width: ' + percent + '%;')
                .attr('aria-valuenow', percent)

            startTime = currentTime;

            downloadText.innerHTML = `Extraction effectuer à ${percent}%`
            if (percent <= 33) {
                progressBar.addClass('bg-danger')
            } else if (percent >= 34 && percent <= 66) {
                progressBar.removeClass('bg-danger')
                progressBar.addClass('bg-warning')
            } else {
                progressBar.removeClass('bg-warning')
                progressBar.addClass('bg-success')
            }
        }
    })

    extract.on('error', (error) => {
        console.error(error);
        downloadText.innerHTML = '<i class="fa fa-times text-danger"></i> Erreur lors de l\'extraction du fichier '+nameZip
    });

    extract.on('end', () => {
        downloadText.innerHTML = '<i class="fa fa-check"></i> Extraction terminer'
        installRoute(route_id)
    });


}

function installRoute(route_id) {
    let downloadText = document.querySelector('.download-text')

    downloadText.innerHTML = '<i class="fa fa-spinner fa-spin"></i> Installation de la route en cours...'

    if(!store.get('trainz_path')) {
        downloadText.innerHTML = "<i class='icon-warning text-warning'></i> Dossier non rensigner<br><i>Veuillez configurer le dossier d'installation de trainz <strong>Paramètres -> Trainz</strong></i>"
    } else {
        const ps = new Shell({
            executionPolicy: 'Bypass',
            noProfile: true
        });

        startTainz().then(result => {
            // Listing des fichiers cdp du dossier tmp
            fs.readdir(`${app.getPath('userData')}/routes/${route_id}/tmp`, {}, (err, files) => {
                if(err) throw err;
                Array.from(files).forEach((file) => {
                    ps.addCommand(`cd '${store.get('trainz_path')}'`)
                    ps.addCommand(`./trainzutil.exe installCDP '${app.getPath('userData')}/routes/${route_id}/tmp`)
                })
            })

            ps.invoke().then(output  => {
                commitCDP(route_id)
            }).catch(error => {
                console.error(error)
                downloadText.innerHTML = `<i class="icon-warning text-warning"></i> Veuillez lancer l'executable du jeux (ex: TRS19.exe)`
            })
        }).catch(error => {
            throw error;
        });

    }
}

function commitCDP(route_id) {
    let downloadText = document.querySelector('.download-text')

    const ps = new Shell({
        executionPolicy: 'Bypass',
        noProfile: true
    });

    downloadText.innerHTML = `<i class="fa fa-spinner fa-spin"></i> Vérification de l'objet`
    let database;
    db.getRows('routes', {identifier: route_id}, (succ, result) => {
        database = result[0]
    })

    let kuids = [
        database.routeKuid,
        database.dependanceKuid
    ];

    Array.from(kuids).forEach((kuid) => {
        ps.addCommand(`cd '${store.get('trainz_path')}'`)
        ps.addCommand(`./trainzutil.exe commit '<${kuid}>'`)
        ps.invoke().then(response => {

            // Suppression du dossier & Fichier de l'objets
            downloadText.innerHTML = `<i class="fa fa-spinner fa-spin"></i> Suppression du cache système`
            fs.rmdir(`${app.getPath('userData')}/routes/${route_id}/tmp/`, {recursive: true}, err => {
                if (err) throw err;
            })

            fs.readdir(`${app.getPath('userData')}/routes/${route_id}/tmp`, {}, (err, files) => {
                Array.from(files).forEach((file) => {
                    fs.unlink(`${app.getPath('userData')}/routes/${route_id}/tmp/${file}`, err => {
                        if(err) throw downloadText.innerHTML = `<i class="fa fa-times text-danger"></i> Erreur lors de la suppression des caches du systèmes`
                    })
                })
            })

            fs.unlink(`${app.getPath('userData')}/routes/${route_id}/route_${database.version}_${database.build}.zip`, err => {
                if (err) throw downloadText.innerHTML = `<i class="fa fa-times text-danger"></i> Erreur lors de la suppression des caches du systèmes`
            })

            // Enregistrement en base de donnée
            downloadText.innerHTML = `<i class="fa fa-spinner fa-spin"></i> Enregistrement en base de donnée`
            let where = {
                'identifier': route_id
            }

            let set = {
                'installed': true,
                'installed_at': now,
                'installed_version': database.serveur_version
            }

            db.updateRow('assets', where, set, (succ, err) => {

            })

            downloadText.innerHTML = `<i class="icon-check"></i> Installation terminer`
        }).catch(err => {
            downloadText.innerHTML = `<i class="icon-warning1 text-danger"></i> Impossible d'installer la route.`
            console.log(err.toString())
        })
    })
}

function startTainz() {
    const ps = new Shell({
        executionPolicy: 'Bypass',
        noProfile: true
    });

    ps.addCommand(`cd '${store.get('trainz_path')}'`)
    ps.addCommand(`./TRS19.exe`)

    return ps.invoke()
}

loadListRoute()
