const PerfectScrollbar = require('perfect-scrollbar')
const tippy = require('tippy.js')
const {DownloaderHelper} = require('node-downloader-helper');

const ps = new PerfectScrollbar(document.querySelector('.download_aside_container', '#category_content'))
const ps2 = new PerfectScrollbar(document.querySelector('#category_content'))
ps.update();
ps2.update();

let ElementsDownload = {
    downloadSelectorContainer: document.querySelector('.download_selector'),
    downloadCategoryContent: document.querySelector('#category_content'),
    loadingAsset: utils.getId('loadingAsset'),
}

function block() {
    ElementsRoute.loadingRoad.innerHTML = `
    <div class="d-flex justify-content-center">
        <div class="spinner-border text-primary" style="width: 5rem; height: 5rem;" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
    `
}

function unblock() {
    ElementsRoute.loadingRoad.style.display = 'none'
}

function loadCategories() {
    utils.block()
    $.ajax({
        url: store.get('API_ENDPOINT') + 'launcher/asset/category/list',
        success: (data) => {
            utils.unblock()
            Array.from(data.data).forEach((item) => {
                ElementsDownload.downloadSelectorContainer.innerHTML += `
                <a href="" class="nav-link process active category-links" data-id="${item.id}" data-toggle="tooltip" title="${item.name}">
                    <img src="${store.get('DOWN_ENDPOINT')}assets/category/${item.id}.png" alt="">
                </a>
                `;
            })

            let links = document.querySelectorAll('.category-links')
            Array.from(links).forEach((link) => {
                link.addEventListener('click', (e) => {
                    e.preventDefault();
                    loadAssets(link.dataset.id)
                })
            })
        },
        error: error => {
            notify.errorRestart(error.responseText)
        }
    })
}

function openModal(evt, asset_id) {
    let modal = $("#modalAsset");
    modal.modal('show')
    modal.find('#modal_contenue').html(`<div class="d-flex justify-content-center">
          <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
            <span class="sr-only">Loading...</span>
          </div>
        </div>`)

    $.ajax({
        url: store.get("API_ENDPOINT") + 'launcher/asset/' + asset_id,
        success: (data) => {
            let img;
            let item = data.data
            console.log(item)
            let meshes = {
                0: {"content": ""},
                1: {"content": '<button class="btn btn-outline-light mr-1"><i class="fa fa-cubes"></i></button> <span class="mr-1">|</span>'}
            }
            let social = {
                0: {"content": ""},
                1: {"content": '<a class="btn-link text-white"><i class="icon-twitter fa-lg mr-1"></i></a><a class="btn-link text-white"><i class="icon-facebook fa-lg mr-1"></i></a>'}
            }
            let pricing = {
                0: {"content": "Gratuit", "class": "text-success"},
                1: {"content": item.price + " €", "class": "text-danger"},
            }

            modal.find('.modal-title').html(`<img src="${store.get('DOWN_ENDPOINT')}assets/category/${item.category.id}.png" style="width: 30px; height: 30px;" alt/> ${item.designation} / <small class="text-muted">${item.category.name}</small>`)
            modal.find('.modal-body-asset').html(`
            <img src="${item.image}" class="img-fluid" alt="">
                <div class="asset-action-bar">
                    <div class="row">
                        <div class="col-md-4">
                            ${meshes[item.meshes].content}
                            ${social[item.social].content}
                        </div>
                        <div class="col-md-4"> 
                            <span class="text-center download-text"></span>
                        </div>
                        <div class="col-md-4 text-right">
                            <button class="btn btn-success shadow btn-download" data-uuid="${item.uuid}"><i class="icon-download"></i> Télécharger</button>
                        </div>
                    </div>
                </div>
                <div id="progress-body">
                    <div class="progress" style="height: 5px;">
                        <div class="progress-bar bg-info" role="progressbar" style="width: 0;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
                <div class="asset-content">
                    ${item.description}
                </div>
                <div class="row asset-spec">
                    <div class="col-md-5">
                        <div class="card asset-info">
                            <table class="table text-white">
                                <tbody>
                                <tr>
                                    <td class="font-weight-bold">Date de publication</td>
                                    <td class="text-right">${item.published_at.normalize} (${item.published_at.human})</td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold">Prix</td>
                                    <td class="text-right">${pricing[item.pricing].content}</td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold">Nombre de téléchargement</td>
                                    <td class="text-right">${item.count_download}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-5">
                        <div class="card asset-kuid">
                            <div class="card-body">
                                <h5 class="card-title">Liste des KUID</h5>
                                <ul id="listKuid">
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            `)
            listKuid(item.kuids)
            downloadAsset(item.id, item.uuid, item.last_kuid)
        }
    })
}

function listKuid(kuids) {
    let ul = document.querySelector("#listKuid")
    Array.from(kuids).forEach((kuid) => {
        ul.innerHTML += `<li>${kuid.kuid}</li>`
    })
}

function loadAssets(category_id) {
    block()
    ElementsDownload.downloadCategoryContent.innerHTML = null;
    $.ajax({
        url: store.get('API_ENDPOINT') + 'launcher/asset/category/' + category_id,
        success: (data) => {
            unblock()
            if (data.data.assets.count === 0) {

            } else {
                Array.from(data.data.assets.data).forEach((asset) => {
                    let img;
                    let pricing = {
                        0: {"title": "Gratuit", "class": "text-success"},
                        1: {"title": asset.price + " €", "class": "text-danger"},
                    };
                    let actions = {
                        0: {title: "Télécharger", "class": "success", "icon": "icon-download"},
                        1: {title: "Acheter", "class": "danger", "icon": "icon-add_shopping_cart"},
                    }
                    ElementsDownload.downloadCategoryContent.innerHTML += `
                            <div class="card mb-3" style="background-color: rgba(109,109,109,0.87); color: white">
                                <div class="row no-gutters">
                                    <div class="col-md-4">
                                        <img src="${asset.image}" class="card-img" alt="...">
                                    </div>
                                    <div class="col-md-8">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-9">
                                                    <h3 class="card-title">${asset.designation}</h3>
                                                    ${asset.short_description}
                                                </div>
                                                <div class="col-md-3 text-right">
                                                    <span class="font-weight-bold h3 ${pricing[asset.pricing].class}">${pricing[asset.pricing].title}</span>
                                                </div>
                                            </div>
                                            <div id="progress">
                                                <div class="progress" style="height: 5px;">
                                                    <div class="progress-bar bg-info" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer" style="position: absolute;bottom: 0;width: 100%;background-color: rgba(0, 0, 0, 0.50);">
                                            <div class="row">
                                                <div class="col-md-6"><i class="icon-download3"></i> ${asset.count_download}</div>
                                                <div class="col-md-6 text-right">
                                                    <button 
                                                    data-id="${asset.id}" 
                                                    data-pricing="${asset.pricing}"
                                                    class="btn btn-sm btn-${actions[asset.pricing].class} btn-download-action"><i class="${actions[asset.pricing].icon}"></i> ${actions[asset.pricing].title}</button>  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                })
                redirectUri()
            }
        }
    })
}

function redirectUri() {
    let assets = document.querySelectorAll('.btn-download-action')
    Array.from(assets).forEach((btn) => {
        btn.addEventListener('click', (event) => {
            console.log(btn.dataset.pricing, btn.dataset.id)
            if (btn.dataset.pricing === '0') {
                openModal(event, btn.dataset.id)
            } else {
                utils.openExternalShell(store.get('SITE_ENDPOINT') + 'download/' + btn.dataset.id)
            }
        })
    })
}

function byteHelper(value) {
    if (value === 0) {
        return '0 b';
    }
    const units = ['b', 'kB', 'MB', 'GB', 'TB'];
    const number = Math.floor(Math.log(value) / Math.log(1024));
    return (value / Math.pow(1024, Math.floor(number))).toFixed(1) + ' ' +
        units[number];
}

function pauseResumeTimer(_dl, wait) {
    setTimeout(() => {
        _dl.pause()
            .then(() => console.log(`Paused for ${wait / 1000} seconds`))
            .then(() => setTimeout(() => _dl.resume(), wait));

    }, wait);
}

function downloadAsset(asset_id, uuid, kuid) {
    let btns = document.querySelectorAll('.btn-download')
    Array.from(btns).forEach((btn) => {
        btn.addEventListener('click', () => {
            let startTime = new Date();
            process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;
            const uri = store.get('DOWN_ENDPOINT') + 'assets/' + asset_id + '/' + uuid + '.zip'
            const dl = new DownloaderHelper(uri, app.getPath('userData') + '/assets/', {
                override: true
            })

            let progressBody = document.querySelector('#progress-body')
            let downloadText = document.querySelector('.download-text')
            let progressBar = $(".progress-bar")

            dl.start()
            dl
                .on('download', downloadInfo => {


                    btn.innerHTML = '<i class="fa fa-spinner fa-spin"></i> Téléchargement en cours...'
                    btn.setAttribute('disabled', 'true')
                    downloadText.innerHTML = 'Téléchargement en cours...'

                    progressBody.style.display = 'block'
                    progressBar
                        .attr('style', 'width: 0;')
                        .attr('aria-valuenow', 0)
                        .removeClass('bg-info')
                })
                .on('end', downloadInfo => {
                    btn.innerHTML = '<i class="icon-download"></i> Télécharger'
                    btn.removeAttribute('disabled')

                    progressBody.style.display = 'none'

                    downloadText.innerHTML = '<i class="icon-check"></i> Téléchargement terminer'

                    addCounterDownload(asset_id)

                    setTimeout(() => {
                        downloadText.innerHTML = '<i class="fa fa-spinner fa-spin"></i> Extraction de l\'objet ...'
                        dezipAsset(uuid, kuid)
                    }, 1500)

                })
                .on('skip', skipInfo => {
                    btn.innerHTML = '<i class="icon-download"></i> Télécharger'
                    btn.removeAttribute('disabled')

                    progressBody.style.display = 'none'

                    downloadText.innerHTML = '<i class="icon-arrow-left"></i> Téléchargement Interrompu'

                    let msg = {
                        title: 'Téléchargement stoppée',
                        message: `Le fichier ${skipInfo.fileName} existe déja !`,
                        width: 400,
                        timeout: 6000,
                        focus: true // set focus back to main window
                    }
                    ipc.send('electron-toaster-message', msg)
                })
                .on('error', err => {
                    btn.innerHTML = '<i class="icon-download"></i> Télécharger'
                    btn.removeAttribute('disabled')

                    progressBody.style.display = 'none'

                    downloadText.innerHTML = '<i class="icon-warning"></i> Erreur de téléchargement'

                    let msg = {
                        title: 'Erreur de téléchargement',
                        message: `Erreur: ${err}`,
                        width: 400,
                        timeout: 6000,
                        focus: true // set focus back to main window
                    }
                    ipc.send('electron-toaster-message', msg)
                })
                .on('retry', (attempt, opts) => {
                    console.log(
                        'Retry Attempt:', attempt + '/' + opts.maxRetries,
                        'Starts on:', opts.delay / 1000, 'secs'
                    );
                })
                .on('resume', isResumed => {
                    // is resume is not supported,
                    // a new pipe instance needs to be attached
                    if (!isResumed) {
                        dl.unpipe();
                        console.warn("This URL doesn't support resume, it will start from the beginning");
                    }
                })
                .on('stateChanged', state => console.log('State: ', state))
                .on('renamed', filePaths => console.log('File Renamed to: ', filePaths.fileName))
                .on('progress', stats => {
                    const progress = stats.progress.toFixed(1);
                    const speed = byteHelper(stats.speed);
                    const downloaded = byteHelper(stats.downloaded);
                    const total = byteHelper(stats.total);

                    // print every one second (`progress.throttled` can be used instead)


                    const currentTime = new Date();
                    const elaspsedTime = currentTime - startTime;
                    if (elaspsedTime > 1000) {
                        progressBar
                            .attr('style', 'width: ' + progress + '%;')
                            .attr('aria-valuenow', progress)

                        startTime = currentTime;
                        console.log(`${speed}/s - ${progress}% [${downloaded}/${total}]`);
                        downloadText.innerHTML = `${downloaded} sur ${total} à ${speed}/s`
                        if (progress <= 33) {
                            progressBar.addClass('bg-danger')
                        } else if (progress >= 34 && progress <= 66) {
                            progressBar.removeClass('bg-danger')
                            progressBar.addClass('bg-warning')
                        } else {
                            progressBar.removeClass('bg-warning')
                            progressBar.addClass('bg-success')
                        }
                    }
                });

            console.log('Downloading: ', uri);
        })
    })
}

function dezipAsset(uuid, kuid) {
    let zip = new AdmZip(app.getPath('userData') + '/assets/' + uuid + '.zip')
    let entries = zip.getEntries();
    let downloadText = document.querySelector('.download-text')

    entries.forEach((entry) => {
        console.log(entry.toString());
    })

    zip.extractAllToAsync(app.getPath('userData') + '/assets/' + uuid, true, (event, data) => {
        downloadText.innerHTML = `<i class="icon-check"></i> Extraction terminer`

        setTimeout(() => {
            downloadText.innerHTML = "<i class='fa fa-spinner fa-spin'></i> Installation de l'objet en cours..."
            installAsset(uuid, kuid)
        }, 1000)

    })
}

function installAsset(uuid, kuid) {
    let downloadText = document.querySelector('.download-text')
    // On vérifie que l'utilisateur à renseigner le dossier d'installation
    if (!store.get('trainz_path')) {
        downloadText.innerHTML = "<i class='icon-warning text-warning'></i> Dossier non rensigner<br><i>Veuillez configurer le dossier d'installation de trainz <strong>Paramètres -> Trainz</strong></i>"
    } else {
        const ps = new Shell({
            executionPolicy: 'Bypass',
            noProfile: true
        });

        // Liste des objets du dossier uuid
        fs.readdir(app.getPath('userData') + '/assets/' + uuid, {}, (err, files) => {
            if (err) throw err;
            Array.from(files).forEach((file) => {
                console.log(file.toString())
                ps.addCommand(`cd '${store.get('trainz_path')}'`)
                ps.addCommand(`./trainzutil.exe installCDP '${app.getPath('userData')}/assets/${uuid}`)
            })
        })


        ps.invoke().then(response => {
            console.log(response)
            commitCdp(uuid, kuid)
        }).catch(error => {
            downloadText.innerHTML = `<i class="icon-warning text-warning"></i> Veuillez lancer l'executable du jeux (TRS19.exe)`
            console.error(error)
        })
    }
}

function commitCdp(uuid, kuid) {
    let downloadText = document.querySelector('.download-text')
    const ps = new Shell({
        executionPolicy: 'Bypass',
        noProfile: true
    });
    downloadText.innerHTML = `<i class="fa fa-spinner fa-spin"></i> Vérification de l'objet`
    ps.addCommand(`cd '${store.get('trainz_path')}'`)
    ps.addCommand(`./trainzutil.exe commit '<${kuid}>'`)
    ps.invoke().then(response => {
        console.log(response)

        // Suppression du dossier & Fichier de l'objets
        downloadText.innerHTML = `<i class="fa fa-spinner fa-spin"></i> Suppression du cache système`
        fs.rmdir(app.getPath('userData') + '/assets/' + uuid, {recursive: true}, err => {
            if (err) throw err;
        })
        fs.unlink(app.getPath('userData') + '/assets/' + uuid + '.zip', err => {
            if (err) throw err;
        })

        // Enregistrement en base de donnée
        downloadText.innerHTML = `<i class="fa fa-spinner fa-spin"></i> Enregistrement en base de donnée`
        let where = {
            'uuid': uuid
        }

        let set = {
            'installed': true
        }

        db.updateRow('assets', where, set, (succ, err) => {
            console.log('success: '+succ+' / '+err)
        })

        downloadText.innerHTML = `<i class="icon-check"></i> Installation terminer`
    }).catch(err => {
        downloadText.innerHTML = `<i class="icon-warning1 text-danger"></i> Impossible d'installer l'objet.`
        console.log(err.toString())
    })

}

function addCounterDownload(asset_id) {
    $.ajax({
        url: `${store.get('API_ENDPOINT')}launcher/asset/${asset_id}/addCounter`,
        success: (data) => {
            console.log(data)
        },
        error: (err) => {
            console.log(err);
        }
    })
}

loadCategories()
