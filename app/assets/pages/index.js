//const Utils = require('../../assets/scripts/Utils/Utils.js')
//const utils = new Utils();
const toastr = require('toastr')
//const store = new Store();

require('dotenv').config({ path: './app/.env' });

let ElementsIndex = {
    newsWrapper: utils.getId('newsWrapper'),
    listArticle: utils.getId('listArticle'),
    modalArticle: utils.getId('modalArticle'),
    formSettingTrainz: $("#formSettingTrainz")
}

function fillParameter() {
    ElementsIndex.formSettingTrainz.find('.actually_build').html(store.get('trainz_version'))
    ElementsIndex.formSettingTrainz.find('.actually_path').html(store.get('trainz_path'))
}

function openTab(evt, tabName) {
    let i, tabcontent, tablinks
    tabcontent = document.getElementsByClassName("tabcontent")
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = 'none'
    }
    tablinks = document.getElementsByClassName("nav-link")
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(tabName).style.display = "block";
    evt.currentTarget.className += " active";
}

function loadNewsWrapper() {
    utils.block()
    $.ajax({
        url: store.get("API_ENDPOINT")+'launcher/blog/list',
        method: "POST",
        data: {"limit": 4},
        success: (data) => {
            utils.unblock()
            Array.from(data.data).forEach((item) => {
                ElementsIndex.newsWrapper.innerHTML += `
                <div class="col-md-3">
                    <div class="card card-transparent" onclick="openModal(event, ${item.id})">
                        <img src="${item.image}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <small>${item.category.name}</small>
                            <h5 class="card-title">${item.title}</h5>
                        </div>
                    </div>
                </div>
                `
            })
        },
        error: (error) => {
            utils.unblock()
            utils.getId('loadingState').innerHTML = '<i class="fa fa-exclamation-circle fa-lg text-danger" data-toggle="tooltip" title="Erreur lors de la récupération des informations !"></i>'
            notify.errorRestart(error.responseText)
        }
    })
}

function loadListArticle() {
    utils.block()
    $.ajax({
        url: store.get("API_ENDPOINT")+'launcher/blog/list',
        method: "POST",
        data: {"limit": 10},
        success: (data) => {
            utils.unblock()
            Array.from(data.data).forEach((item) => {
                ElementsIndex.listArticle.innerHTML += `
                <li class="articleListItem" onclick="openModal(event, ${item.id})">
                    <div class="card card-transparent">
                        <div class="row no-gutters">
                            <div class="col-md-3">
                                <img src="${item.image}" class="card-img" alt="...">
                            </div>
                            <div class="col-md-9">
                                <div class="card-body">
                                    <small>${item.category.name}</small>
                                    <h5 class="card-title">${item.title}</h5>
                                    ${item.short_content}
                                </div>
                                <div class="card-footer">
                                    ${item.published_at}
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                `
            })
        },
        error: (error) => {
            utils.unblock()
            utils.getId('loadingState').innerHTML = '<i class="fa fa-exclamation-circle fa-lg text-danger" data-toggle="tooltip" title="Erreur lors de la récupération des informations !"></i>'
            notify.errorRestart(error.responseText)
        }
    })
}

function openModal(evt, article_id) {
    let modal = $("#modalArticle");
    modal.modal('show')
    modal.find('#modal_contenue').html(`<div class="d-flex justify-content-center">
          <div class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
            <span class="sr-only">Loading...</span>
          </div>
        </div>`)

    $.ajax({
        url: store.get("API_ENDPOINT")+'launcher/blog/'+article_id,
        success: (data) => {
            let item = data.data
            modal.find('#modal_contenue').html(item.content)
            modal.find('.modal-title').html(`${item.title} . <small class="text-muted">${item.category.name}</small>`)
            modal.find('.modal_img').html(`<img src="${store.get("SITE_ENDPOINT")}storage/blog/${item.id}.png" class="img-fluid">`)
            modal.find('.modal_date').html(item.published_at)
            if(item.social === true) {
                modal.find('.modal_social').html(`
                | <a href=""><i class="fa fa-twitter fa-lg text-primary" </a> <a href=""><i class="fa fa-facebook fa-lg text-primary" </a>
                `)
            }
            modal.find('.modal-footer').html(`<a class="btn btn-primary btn-linker" data-href="${store.get("SITE_ENDPOINT")}blog/${item.slug}">Voir en ligne</a>`)
            $(".btn-linker").on('click', () => {
                utils.openExternalShell($(".btn-linker").attr('data-href'))
            })
        }
    })
}

// Poste de la fonction remarque
$("#formNoticeAccept").on('submit', (e) => {
    e.preventDefault();
    let form = $("#formNoticeAccept")
    let button = form.find('.btn-primary')
    let data = {
        "channel": "Launcher",
        "categorie": "Accept",
        "sector": $("#acceptChoice").val(),
        "message": $("#acceptDesc").val()
    }

    button.html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Chargement').attr('disabled', true)

    $.ajax({
        url: store.get("API_ENDPOINT")+'launcher/notice',
        method: "POST",
        data: data,
        success: () => {
            form[0].reset()
            button.html('Envoyer').removeAttr('disabled')
            toastr.success("Votre message à bien été envoyer", process.env.APPLICATION)
            $("#modalFormAccept").modal('hide')
        },
        error: (err) => {
            button.html('Envoyer').removeAttr('disabled')
            toastr.error("Erreur lors de l'envoie de votre message", process.env.APPLICATION)
            fs.writeFileSync('console.log', console.error(err.responseText))
        },
        always: () => {
            button.html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Chargement').attr('disabled', true)
        }
    })
})
$("#formNoticeDecease").on('submit', (e) => {
    e.preventDefault();
    let form = $("#formNoticeDecease")
    let button = form.find('.btn-primary')
    let data = {
        "channel": "Launcher",
        "categorie": "Decease",
        "sector": $("#desceaseChoice").val(),
        "message": $("#deceaseDesc").val()
    }

    button.html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Chargement').attr('disabled', true)

    $.ajax({
        url: store.get("API_ENDPOINT")+'launcher/notice',
        method: "POST",
        data: data,
        success: () => {
            form[0].reset()
            button.html('Envoyer').removeAttr('disabled')
            toastr.success("Votre message à bien été envoyer", process.env.APPLICATION)
            $("#modalFormDecease").modal('hide')
        },
        error: (err) => {
            button.html('Envoyer').removeAttr('disabled')
            toastr.error("Erreur lors de l'envoie de votre message", process.env.APPLICATION)
            fs.writeFileSync('console.log', console.error(err.responseText))
        },
        always: () => {
            button.html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Chargement').attr('disabled', true)
        }
    })
})
$("#formNoticeSuggest").on('submit', (e) => {
    e.preventDefault();
    let form = $("#formNoticeSuggest")
    let button = form.find('.btn-primary')
    let data = {
        "channel": "Launcher",
        "categorie": "Suggest",
        "sector": $("#suggestChoice").val(),
        "message": $("#suggestDesc").val()
    }

    button.html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Chargement').attr('disabled', true)

    $.ajax({
        url: store.get("API_ENDPOINT")+'launcher/notice',
        method: "POST",
        data: data,
        success: () => {
            form[0].reset()
            button.html('Envoyer').removeAttr('disabled')
            toastr.success("Votre message à bien été envoyer", process.env.APPLICATION)
            $("#modalFormSuggest").modal('hide')
        },
        error: (err) => {
            button.html('Envoyer').removeAttr('disabled')
            toastr.error("Erreur lors de l'envoie de votre message", process.env.APPLICATION)
            fs.writeFileSync('console.log', console.error(err.responseText))
        },
        always: () => {
            button.html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Chargement').attr('disabled', true)
        }
    })
})
$("#formNoticeBug").on('submit', (e) => {
    e.preventDefault();
    let form = $("#formNoticeBug")
    let button = form.find('.btn-primary')
    let data = {
        "channel": "Launcher",
        "categorie": "Bug",
        "sector": $("#bugChoice").val(),
        "message": $("#bugDesc").val()
    }

    button.html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Chargement').attr('disabled', true)

    $.ajax({
        url: store.get("API_ENDPOINT")+'launcher/notice',
        method: "POST",
        data: data,
        success: () => {
            form[0].reset()
            button.html('Envoyer').removeAttr('disabled')
            toastr.success("Votre message à bien été envoyer", process.env.APPLICATION)
            $("#modalFormBug").modal('hide')
        },
        error: (err) => {
            button.html('Envoyer').removeAttr('disabled')
            toastr.error("Erreur lors de l'envoie de votre message", process.env.APPLICATION)
            fs.writeFileSync('console.log', console.error(err.responseText))
        },
        always: () => {
            button.html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Chargement').attr('disabled', true)
        }
    })
})

// Poste de la fonction paramètre
    // Trainz Config
ElementsIndex.formSettingTrainz.on('submit', (e) => {
    e.preventDefault()
    let form = $("#formSettingTrainz")
    let button = form.find('.btn-primary')

    button.html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Chargement').attr('disabled', true)

    store.set('trainz_version', $("#trainz_version").val())
    store.set('trainz_path', $("#trainz_path").val())

    if(store.get('trainz_version') !== null && store.get('trainz_path') !== null) {
        button.html('Enregistrer').removeAttr('disabled')
        toastr.success("Vos paramètres ont été enregistrer")

        form.find('.actually_build').html(store.get('trainz_version'))
        form.find('.actually_path').html(store.get('trainz_path'))
    } else {
        button.html('Enregistrer').removeAttr('disabled')
        toastr.error("Erreur lors de l'enregistrement de vos paramètres")
    }
})



loadNewsWrapper();
loadListArticle();
fillParameter()