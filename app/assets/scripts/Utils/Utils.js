const Pusher = require('pusher-js');
const {ipcRenderer, shell} = require('electron')
const yaml = require('js-yaml');
require('dotenv').config({ path: './app/.env' });

class Utils {
    constructor() {
        this.endpoint = null;
        this.debug = /--debug/.test(process.argv[2])
        this.storagePath = this.storagePathEnv()
        this.site = this.siteEnv(this.debug)

        this.endPointEnv(process.env.DEBUG)
    }

    endPointEnv(debug) {
        if (debug === false) {
            return "https://api.trainznation.test/api/"
        } else {
            return "https://api.trainznation.tk/api/"
        }
    }

    siteEnv(debug) {
        if (this.debug) {
            return "https://api.trainznation.test/"
        } else {
            return "https://api.trainznation.tk/"
        }
    }

    storagePathEnv() {
        if (this.debug) {
            return "https://trainznation.test/storage/"
        } else {
            return "https://trainznation.tk/storage/"
        }
    }

    getEndpoint() {
        return this.endPointEnv(process.env.DEBUG);
    }

    getStoragePath() {
        return this.storagePath;
    }

    getSitePath() {
        return this.storagePath;
    }

    getId(id) {
        return document.getElementById(id)
    }

    debug() {
        return /--debug/.test(process.argv[2]);
    }

    CallAjaxQuery(uri, method, data) {
        this.block()
        $.ajax({
            url: this.endpoint + uri,
            method: method,
            timeout: 0,
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            data: JSON.stringify(data),
            success: (data) => {
                this.unblock()
                return data;
            }
        })
    }

    testImg(uri) {
        $.ajax({
            url: uri,
            success: () => {
                return true;
            },
            error: () => {
                return false;
            }
        })
    }

    block() {
        this.getId('loadingState').innerHTML = `
        <div class="d-flex justify-content-center">
          <div class="spinner-border text-primary" role="status">
            <span class="sr-only">Loading...</span>
          </div>
        </div>
        `
    }

    unblock() {
        this.getId('loadingState').innerHTML = null;
    }

    verifMajBlog() {
        Pusher.logToConsole = true;

        let pusher = new Pusher('4c1ef7368bab4be13d34', {
            cluster: 'eu'
        })

        let channel = pusher.subscribe('interlocking-channel')
        channel.bind('new-blog', (data) => {
            const notification = {
                title: "Nouvelle article publier",
                body: `${data.title}`,
                icon: path.join(__dirname, '../images/logo_carre.png')
            };

            new window.Notification(notification.title, notification);
        })
    }

    verifMaj(localVersion) {
        $.ajax({
            url: 'https://download.trainznation.tk/apps/launcher/latest.yml',
            success: (data) => {
                return data
            },
            error: err => {
                notify.errorRestart(err)
            }
        })
    }

    openExternalShell(href) {
        shell.openExternal(href)
    }
}


module.exports = Utils;