class Menu {
    constructor() {
        this.winOpen = false;
        this.win = null
    }

    createWindow(nameWindow, title, parentId, debug = false, uri, data = []) {
        if(this.winOpen) {
            return
        }

        this.winOpen = true;

        this.win = new BrowserWindow({
            width: 1250,
            height: 500,
            title: title,
            show: false,
            frame: false,
            parent: BrowserWindow.fromId(parentId),
            modal: true,
            webPreferences: {
                nodeIntegration: true,
                contextIsolation: false,
                enableRemoteModule: true,
                worldSafeExecuteJavaScript: true
            }
        })

        if(debug) {
            this.win.webContents.openDevTools();
        }

        this.win.on('ready-to-show', () => {
            this.win.moveTop();
            this.win.show()
        });

        this.win.on('closed', () => {
            this.win = null;
            this.winOpen = false
        });

        this.win.loadFile(uri)

        this.win.webContents.on('did-finish-load', () => {
            ipcRenderer.send('request-asset-data', data)
        })
    }

}
module.exports = Menu;
