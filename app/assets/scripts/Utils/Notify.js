const Swal = require('sweetalert2')

class Notify {
    constructor() {

    }

    errorRestart(text) {
        Swal.fire({
            title: "Erreur",
            text: text,
            icon: 'error',
            confirmButtonColor: '#3085d6',
            confirmButtonText: `Redemarrer l'application`
        })
    }

    error(text) {
        Swal.fire({
            title: "Erreur",
            text: text,
            icon: 'error',
        })
    }

    warning(title, text) {
        Swal.fire({
            title: title,
            text: text,
            icon: 'warning',
        })
    }

    success(title, text) {
        Swal.fire({
            title: title,
            text: text,
            icon: 'success',
        })
    }

    info(title, text) {
        Swal.fire({
            title: title,
            text: text,
            icon: 'info',
        })
    }

    quest(title, text) {
        Swal.fire({
            title: title,
            text: text,
            icon: 'question',
        })
    }
}

module.exports = Notify