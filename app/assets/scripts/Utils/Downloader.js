const DownloaderShap = require('js-file-downloader')

class Downloader extends DownloaderShap {

    process(event) {
        if(!event.lengthComputable) return;
        let downloadingProgress = Math.floor(event.loaded / event.total * 100)
        console.log(downloadingProgress)
    }

    download(uri) {
        new DownloaderShap({
            url: uri,
            process: process
        }).then((data) => {
            return data;
        }).catch((error) => {
            return error.text()
        })
    }
}

module.exports = Downloader;