const {app, BrowserWindow} = require('electron').remote
const ipc = require('electron').ipcRenderer
const {is, isFirstAppLaunch} = require('electron-util');
const Store = require('electron-store');
const fs = require('fs')
const db = require('electron-db')
const moment = require('moment')


const now = moment().format('YYYY-MM-DD hh:mm:ss')
let chargingText = document.querySelector('.chargingElement')
let store;
let terminate = 0;

function storingDatabse() {
    chargingText.innerHTML = '<i class="fa fa-spinner fa-lg fa-spin"></i> Initialisation de la configuration...'

}

function createPath() {
    chargingText.innerHTML = '<i class="fa fa-spinner fa-lg fa-spin"></i> Création des dossiers nécessaires à l\'application'
    // Création des Elements & Dossier
    fs.mkdir(app.getPath('userData') + '/assets/', {recursive: true}, (err) => {
        if (err) throw chargingText.innerHTML = '<i class="fa fa-warning fa-lg text-danger"></i> '+err;
    });
    fs.mkdir(app.getPath('userData') + '/routes/', {recursive: true}, (err) => {
        if (err) throw chargingText.innerHTML = '<i class="fa fa-warning fa-lg text-danger"></i> '+err;
    });
    fs.mkdir(app.getPath('userData') + '/sessions/', {recursive: true}, (err) => {
        if (err) throw chargingText.innerHTML = '<i class="fa fa-warning fa-lg text-danger"></i> '+err;
    });
}

function createDatabase() {
    chargingText.innerHTML = '<i class="fa fa-spinner fa-lg fa-spin"></i> Initialisation de la configuration...'
    const pathAssetDb = app.getPath('userData')
    db.createTable('assets', pathAssetDb, (succ, msg) => {
        if (succ) {
            console.log(msg)
        } else {
            chargingText.innerHTML = '<i class="fa fa-warning fa-lg text-danger"></i> '+msg;
        }
    })
    const pathRouteDb = app.getPath('userData')
    db.createTable('routes', pathRouteDb, (succ, msg) => {
        if (succ) {
            console.log(msg)
        } else {
            chargingText.innerHTML = '<i class="fa fa-warning fa-lg text-danger"></i> '+msg;
        }
    })
    const pathSessionDb = app.getPath('userData')
    db.createTable('sessions', pathSessionDb, (succ, msg) => {
        if (succ) {
            console.log(msg)
        } else {
            chargingText.innerHTML = '<i class="fa fa-warning fa-lg text-danger"></i> '+msg;
        }
    })
}

function insertAssetValue() {
    chargingText.innerHTML = '<i class="fa fa-spinner fa-lg fa-spin"></i> Remplissage de la base: "Asset"'
    $.ajax({
        url: store.get('API_ENDPOINT') + 'launcher/asset/list',
        success: (data) => {
            Array.from(data.data).forEach((item) => {
                let Obj = {
                    uuid: item.uuid,
                    designation: item.designation,
                    updated_at: item.updated_at,
                    installed: 0,
                    installed_at: null
                }

                if (db.valid('assets')) {
                    db.insertTableContent('assets', Obj, (succ, err) => {
                        console.log('Success: ' + succ)
                        console.log('Message: ' + err)
                    })
                }
            })
        },
        error: err => {
            chargingText.innerHTML = '<i class="fa fa-warning fa-lg text-danger"></i> '+err;
        }
    })
}

function insertRouteValue() {
    chargingText.innerHTML = '<i class="fa fa-spinner fa-lg fa-spin"></i> Remplissage de la base: "Route"'
    $.ajax({
        url: `${store.get('API_ENDPOINT')}launcher/route/list`,
        success: (data) => {
            Array.from(data.data).forEach((item) => {
                let Obj = {
                    identifier: item.id,
                    name: item.name,
                    installed: 0,
                    installed_at: null,
                    installed_version: null,
                    serveur_version: 'V.'+item.version+':'+item.build,
                    updated_at: item.updated_at,
                    routeKuid: item.route_kuid,
                    dependanceKuid: item.dependance_kuid,
                    version: item.version,
                    build: item.build
                }

                if (db.valid('routes')) {
                    db.insertTableContent('routes', Obj, (succ, err) => {
                        console.log('Success: ' + succ)
                        console.log('Message: ' + err)
                    })
                }
            })
        },
        error: err => {
            chargingText.innerHTML = '<i class="fa fa-warning fa-lg text-danger"></i> '+err;
        }
    })
}

async function insertSessionValue() {
    chargingText.innerHTML = '<i class="fa fa-spinner fa-lg fa-spin"></i> Remplissage de la base: "Session"'
    await $.ajax({
        url: `${store.get('API_ENDPOINT')}launcher/route/allSession`,
        success: (data) => {
            Array.from(data.data).forEach((item) => {
                let Obj = {
                    identifier: item.id,
                    route_id: item.route_id,
                    published_at: item.published_at,
                    installed: 0,
                    installed_at: null,
                    updated_at: item.updated_at,
                    kuid: item.kuid
                }

                if (db.valid('sessions')) {
                    db.insertTableContent('sessions', Obj, (succ, err) => {
                        console.log('Success: ' + succ)
                        console.log('Message: ' + err)
                    })
                }
            })
        },
        error: err => {
            chargingText.innerHTML = '<i class="fa fa-warning fa-lg text-danger"></i> '+err;
        }
    })
}

function updateAssetValue() {
    chargingText.innerHTML = '<i class="fa fa-spinner fa-lg fa-spin"></i> Mise à jour de la base: "Asset"'
    $.ajax({
        url: store.get('API_ENDPOINT') + 'launcher/asset/list',
        success: (data) => {
            Array.from(data.data).forEach((item) => {

                let row;
                let where;
                let set;
                db.getRows('assets', {uuid: item.uuid}, (succ, result) => { row = result[0] })

                if(row.installed === 1) {
                    if(row.installed_at <= now && row.installed_at > item.updated_at) {
                        where = {"uuid": item.uuid}
                        set = {"updated_at": item.updated_at, "installed": 2}
                    }else {
                        where = {"uuid": item.uuid}
                        set = {"updated_at": item.updated_at}
                    }
                } else {
                    where = {"uuid": item.uuid}
                    set = {"updated_at": item.updated_at}
                }

                db.updateRow('assets', where, set, (succ, msg) => { console.log(msg) })
            })
        },
        error: err => {
            chargingText.innerHTML = '<i class="fa fa-warning fa-lg text-danger"></i> '+err;
        }
    })
}

function updateRouteValue() {
    chargingText.innerHTML = '<i class="fa fa-spinner fa-lg fa-spin"></i> Mise à jour de la base: "Route"'
    $.ajax({
        url: store.get('API_ENDPOINT') + 'launcher/route/list',
        success: (data) => {
            Array.from(data.data).forEach((item) => {
                let row;
                let where;
                let set;
                db.getRows('routes', {identifier: item.id}, (succ, result) => { row = result[0] })

                if(row.installed === 1) {
                    if(row.installed_at <= now && row.installed_at > item.updated_at) {
                        where = {"identifier": item.id}
                        set = {"updated_at": item.updated_at, "installed": 2, "serveur_version": `V.${item.version}:${item.build}`, "version": `${item.version}`, "build": `${item.build}`}
                    }else {
                        where = {"identifier": item.id}
                        set = {"updated_at": item.updated_at, "serveur_version": `V.${item.version}:${item.build}`, "version": `${item.version}`, "build": `${item.build}`}
                    }
                } else {
                    where = {"identifier": item.uuid}
                    set = {"updated_at": item.updated_at, "serveur_version": `V.${item.version}:${item.build}`, "version": `${item.version}`, "build": `${item.build}`}
                }

                db.updateRow('routes', where, set, (succ, msg) => { console.log(msg) })
            })
        },
        error: err => {
            chargingText.innerHTML = '<i class="fa fa-warning fa-lg text-danger"></i> '+err;
        }
    })
}

async function updateSessionValue() {
    chargingText.innerHTML = '<i class="fa fa-spinner fa-lg fa-spin"></i> Mise à jour de la base: "Session"'
    await $.ajax({
        url: store.get('API_ENDPOINT') + 'launcher/route/allSession',
        success: (data) => {
            Array.from(data.data).forEach((item) => {
                let row;
                let where;
                let set;
                db.getRows('sessions', {identifier: item.id}, (succ, result) => { row = result[0] })

                if(row.installed === 1) {
                    if(row.installed_at <= now && row.installed_at > item.updated_at) {
                        where = {"identifier": item.id}
                        set = {"updated_at": item.updated_at, "installed": 2}
                    }else {
                        where = {"identifier": item.id}
                        set = {"updated_at": item.updated_at}
                    }
                } else {
                    where = {"identifier": item.uuid}
                    set = {"updated_at": item.updated_at}
                }

                db.updateRow('sessions', where, set, (succ, msg) => { console.log(msg) })
            })
        },
        error: err => {
            chargingText.innerHTML = '<i class="fa fa-warning fa-lg text-danger"></i> '+err;
        }
    })
}

// Create Folder for downloader système

function createSubFolder() {
    chargingText.innerHTML = '<i class="fa fa-spinner fa-lg fa-spin"></i> Création des sous dossier'
    db.getAll('routes', (succ, result) => {
        Array.from(result).forEach((data) => {
            fs.mkdir(app.getPath('userData') + '/routes/'+data.identifier, {recursive: true}, (err) => {
                if (err) throw chargingText.innerHTML = '<i class="fa fa-warning fa-lg text-danger"></i> '+err;
            });
            fs.mkdir(app.getPath('userData') + '/routes/'+data.identifier+'/tmp', {recursive: true}, (err) => {
                if (err) throw chargingText.innerHTML = '<i class="fa fa-warning fa-lg text-danger"></i> '+err;
            });
        })
    })
}



// Init FirstTime launch App
storingDatabse();
createPath();
if (isFirstAppLaunch === true || !fs.existsSync(app.getPath('userData') + '/assets.json') || !fs.existsSync(app.getPath('userData') + '/routes.json')) {
    createDatabase()
    insertAssetValue()
    insertRouteValue()
    insertSessionValue().then(result => {
        createSubFolder()
        terminate = 1;
    }).catch(err => {
        chargingText.innerHTML = '<i class="fa fa-warning fa-lg text-danger"></i> '+err.responseText;
    })

} else {
    updateAssetValue()
    updateRouteValue()
    updateSessionValue().then(result => {
        createSubFolder()
        terminate = 1
    }).catch(err => {
        chargingText.innerHTML = '<i class="fa fa-warning fa-lg text-danger"></i> '+err.responseText;
    })
}

setInterval(function () {
    if(terminate === 1) {
        ipc.sendSync('loading-terminate', 'ok')
    }else {
        ipc.sendSync('loading-terminate', 'not')
    }
}, 1200)