const {app, BrowserWindow} = require('electron').remote
const ipc = require('electron').ipcRenderer
const {is, isFirstAppLaunch} = require('electron-util');
const Store = require('electron-store');
const fs = require('fs')
const db = require('electron-db')
const moment = require('moment')


const now = moment().format('YYYY-MM-DD hh:mm:ss')
let chargingText = document.querySelector('.chargingElement')
let store;
let terminate = 0;

function textDiffuse(text, template) {
    chargingText.innerHTML = `<i class="fa ${template}"></i> ${text}`
}

function textError(error) {
    chargingText.innerHTML = `<i class="fa fa-time text-danger"></i> ${error}`
}

function preloadApp() {
    terminate = 0;

    // Storing Database
    textDiffuse('Initialisation de la configuration de l\'application', 'fa-spinner fa-spin fa-lg')
    storeDatabase().then(result => {
        store = result

        // Création des dossiers
        textDiffuse(`Création des dossiers nécessaires à l'application`, 'fa-spinner fa-spin fa-lg')
        createPath().then(result => {
            console.log(result)
            if(
                isFirstAppLaunch === true ||
                !fs.existsSync(app.getPath('userData')+'/assets.json') ||
                !fs.existsSync(app.getPath('userData')+'/routes.json') ||
                !fs.existsSync(app.getPath('userData')+'/sessions.json')
            ) {
                // Création des bases de données
                textDiffuse(`Création des bases de données`, 'fa-spinner fa-spin fa-lg')
                createDatabase().then(result => {
                    console.log(result)
                    insertAssetsValue().then(result => {
                        console.log(result)
                        insertRoutesValue().then(result => {
                            console.log(result)
                            insertSessionsValue().then(result => {
                                console.log(result)
                                createSubFolder().then(result => {
                                    console.log(result)
                                    terminate = 1;
                                })
                            }).catch(error => {
                                textError(`Erreur: ${error}`)
                            })
                        }).catch(error => {
                            textError(`Erreur: ${error}`)
                        })
                    }).catch(error => {
                        textError(`Erreur: ${error}`)
                    })
                }).catch(error => {
                    textError(`Erreur lors de la création des bases de données: ${error}`)
                })
            } else {
                updateAssetsValue().then(result => {
                    console.log(result)
                    updateRoutesValue().then(result => {
                        console.log(result)
                        updateSessionsValue().then(result => {
                            console.log(result)
                            createSubFolder().then(result => {
                                console.log(result)
                                terminate = 1
                            })
                        }).catch(err => {
                            textError(`Erreur: ${err}`)
                        })
                    }).catch(err => {
                        textError(`Erreur: ${err}`)
                    })
                }).catch(err => {
                    textError(`Erreur: ${err}`)
                })
            }
        })
    }).catch(error => {
        textError(`Erreur lors de l'initialisation de l'application: ${error}`)
    })
}

async function storeDatabase() {
    let init;
    if (is.development === true) {
        init = await new Store({
            migrations: {
                '0.0.21': store => {
                    store.set('API_ENDPOINT', 'https://api.trainznation.io/api/');
                    store.set('SITE_ENDPOINT', 'https://front.trainznation.io/');
                    store.set('DOWN_ENDPOINT', 'https://download.trainznation.io/v3/')
                }
            }
        });
    } else {
        init = await new Store({
            migrations: {
                '0.0.21': store => {
                    store.set('API_ENDPOINT', 'https://api.trainznation.tk/api/');
                    store.set('SITE_ENDPOINT', 'https://trainznation.tk/');
                    store.set('DOWN_ENDPOINT', 'https://download.trainznation.tk/')
                }
            }
        });
    }

    return init;
}

async function createPath() {
    let init;
    init = await fs.mkdir(app.getPath('userData') + '/assets/', {recursive: true}, (err) => {
        if (err) throw textError('Erreur lors de la création du dossier: Assets');
    });

    init += await fs.mkdir(app.getPath('userData') + '/routes/', {recursive: true}, (err) => {
        if (err) throw textError('Erreur lors de la création du dossier: Routes');
    });

    init += await fs.mkdir(app.getPath('userData') + '/sessions/', {recursive: true}, (err) => {
        if (err) throw textError('Erreur lors de la création du dossier: Sessions');
    });

    return init;
}

async function createDatabase() {
    let init;
    const path = app.getPath('userData')
    init = await db.createTable('assets', path, (succ, msg) => {
        if(succ) {
            console.log(msg)
        } else {
            textError(`${msg}`)
        }
    })

    init += await db.createTable('routes', path, (succ, msg) => {
        if(succ) {
            console.log(msg)
        } else {
            textError(`${msg}`)
        }
    })

    init += await db.createTable('sessions', path, (succ, msg) => {
        if(succ) {
            console.log(msg)
        } else {
            textError(`${msg}`)
        }
    })

    return init;
}

async function insertAssetsValue() {
    textDiffuse(`Remplissage de la table: 'Assets'`, 'fa-spinner fa-lg fa-spin')
    await $.ajax({
        url: store.get('API_ENDPOINT') + 'launcher/asset/list',
        success: (data) => {
            Array.from(data.data).forEach((item) => {
                let Obj = {
                    uuid: item.uuid,
                    designation: item.designation,
                    updated_at: item.updated_at,
                    installed: 0,
                    installed_at: null
                }

                if (db.valid('assets')) {
                    db.insertTableContent('assets', Obj, (succ, err) => {
                        console.log('Success: ' + succ)
                        console.log('Message: ' + err)
                    })
                }
            })
        },
        error: err => {
            textError("Erreur lors du remplissage de la table 'Assets': "+err)
        }
    })
}

async function insertRoutesValue() {
    textDiffuse(`Remplissage de la table: 'Routes'`, 'fa-spinner fa-lg fa-spin')
    await $.ajax({
        url: `${store.get('API_ENDPOINT')}launcher/route/list`,
        success: (data) => {
            Array.from(data.data).forEach((item) => {
                let Obj = {
                    identifier: item.id,
                    name: item.name,
                    installed: 0,
                    installed_at: null,
                    installed_version: null,
                    serveur_version: 'V.'+item.version+':'+item.build,
                    updated_at: item.updated_at,
                    routeKuid: item.route_kuid,
                    dependanceKuid: item.dependance_kuid,
                    version: item.version,
                    build: item.build
                }

                if (db.valid('routes')) {
                    db.insertTableContent('routes', Obj, (succ, err) => {
                        console.log('Success: ' + succ)
                        console.log('Message: ' + err)
                    })
                }
            })
        },
        error: err => {
            textError("Erreur lors du remplissage de la table 'Routes': "+err)
        }
    })
}

async function insertSessionsValue() {
    textDiffuse(`Remplissage de la table: 'Sessions'`, 'fa-spinner fa-lg fa-spin')
    await $.ajax({
        url: `${store.get('API_ENDPOINT')}launcher/route/allSession`,
        success: (data) => {
            Array.from(data.data).forEach((item) => {
                let Obj = {
                    identifier: item.id,
                    route_id: item.route_id,
                    published_at: item.published_at,
                    installed: 0,
                    installed_at: null,
                    updated_at: item.updated_at,
                    kuid: item.kuid
                }

                if (db.valid('sessions')) {
                    db.insertTableContent('sessions', Obj, (succ, err) => {
                        console.log('Success: ' + succ)
                        console.log('Message: ' + err)
                    })
                }
            })
        },
        error: err => {
            textError("Erreur lors du remplissage de la table 'Sessions': "+err)
        }
    })
}

async function updateAssetsValue() {
    textDiffuse(`Mise à jour de la table: 'Assets'`, 'fa-spinner fa-lg fa-spin')
    await $.ajax({
        url: store.get('API_ENDPOINT') + 'launcher/asset/list',
        success: (data) => {
            Array.from(data.data).forEach((item) => {

                let row;
                let where;
                let set;
                db.getRows('assets', {uuid: item.uuid}, (succ, result) => { row = result[0] })

                if(row.installed === 1) {
                    if(row.installed_at <= now && row.installed_at > item.updated_at) {
                        where = {"uuid": item.uuid}
                        set = {"updated_at": item.updated_at, "installed": 2}
                    }else {
                        where = {"uuid": item.uuid}
                        set = {"updated_at": item.updated_at}
                    }
                } else {
                    where = {"uuid": item.uuid}
                    set = {"updated_at": item.updated_at}
                }

                db.updateRow('assets', where, set, (succ, msg) => { console.log(msg) })
            })
        },
        error: err => {
            textError("Erreur lors de la mise à jour de la table 'Assets': "+err)
        }
    })
}

async function updateRoutesValue() {
    textDiffuse(`Mise à jour de la table: 'Routes'`, 'fa-spinner fa-lg fa-spin')
    await $.ajax({
        url: store.get('API_ENDPOINT') + 'launcher/route/list',
        success: (data) => {
            Array.from(data.data).forEach((item) => {
                let row;
                let where;
                let set;
                db.getRows('routes', {identifier: item.id}, (succ, result) => { row = result[0] })

                if(row.installed === 1) {
                    if(row.installed_at <= now && row.installed_at > item.updated_at) {
                        where = {"identifier": item.id}
                        set = {"updated_at": item.updated_at, "installed": 2, "serveur_version": `V.${item.version}:${item.build}`, "version": `${item.version}`, "build": `${item.build}`}

                    }else {
                        where = {"identifier": item.id}
                        set = {"updated_at": item.updated_at, "serveur_version": `V.${item.version}:${item.build}`, "version": `${item.version}`, "build": `${item.build}`}
                    }
                } else {
                    where = {"identifier": item.id}
                    set = {"updated_at": item.updated_at, "serveur_version": `V.${item.version}:${item.build}`, "version": `${item.version}`, "build": `${item.build}`}
                }

                db.updateRow('routes', where, set, (succ, msg) => { console.log(msg) })
            })
        },
        error: err => {
            textError("Erreur lors de la mise à jour de la table 'Routes': "+err)
        }
    })
}

async function updateSessionsValue() {
    textDiffuse(`Mise à jour de la table: 'Sessions'`, 'fa-spinner fa-lg fa-spin')
    await $.ajax({
        url: store.get('API_ENDPOINT') + 'launcher/route/allSession',
        success: (data) => {
            Array.from(data.data).forEach((item) => {
                let row;
                let where;
                let set;
                db.getRows('sessions', {identifier: item.id}, (succ, result) => { row = result[0] })

                if(row.installed === 1) {
                    if(row.installed_at <= now && row.installed_at > item.updated_at) {
                        where = {"identifier": item.id}
                        set = {"updated_at": item.updated_at, "installed": 2}
                    }else {
                        where = {"identifier": item.id}
                        set = {"updated_at": item.updated_at}
                    }
                } else {
                    where = {"identifier": item.id}
                    set = {"updated_at": item.updated_at}
                }

                db.updateRow('sessions', where, set, (succ, msg) => { console.log(msg) })

            })
        },
        error: err => {
            textError("Erreur lors de la mise à jour de la table 'Sessions': "+err)
        }
    })
}

async function createSubFolder() {
    textDiffuse(`Création des sous dossiers`, 'fa-spinner fa-lg fa-spin')
    await db.getAll('routes', (succ, result) => {
        Array.from(result).forEach((data) => {
            fs.mkdir(app.getPath('userData') + '/routes/'+data.identifier, {recursive: true}, (err) => {
                if (err) throw textError(`Error: ${err}`)
            });
            fs.mkdir(app.getPath('userData') + '/routes/'+data.identifier+'/tmp', {recursive: true}, (err) => {
                if (err) throw textError(`Error: ${err}`)
            });
        })
    })
}


preloadApp()

setInterval(function () {
    if(terminate === 1) {
        ipc.sendSync('loading-terminate', 'ok')
    }else {
        ipc.sendSync('loading-terminate', 'not')
    }
}, 1200)