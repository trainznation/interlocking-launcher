const {BrowserWindow, app} = require('electron').remote
const ipc = require('electron').ipcRenderer
const Utils = require('../assets/scripts/Utils/Utils.js')
const Menu = require('../assets/scripts/Utils/Menu.js')
const Swal = require('../assets/scripts/Utils/Notify.js')
const fs = require('fs')
const marked = require('marked')
const path = require('path')
const Store = require('electron-store');
const {is, isFirstAppLaunch} = require('electron-util');
const log = require('electron-log');
const AdmZip = require('adm-zip');
const Shell = require('node-powershell');
const child = require('child_process').execFile
const db = require('electron-db')
const utils = new Utils();
const menu = new Menu();
const notify = new Swal();


let store = new Store();
require('dotenv').config({ path: './app/.env' });

$('[data-toggle="tooltip"]').tooltip()

let Elements = {
    moduleElem: utils.getId('loadModule'),

    btnClose: utils.getId('close'),
    headClose: utils.getId('headClose'),
    minimize:utils.getId('minimize'),
    maximize: utils.getId('maximize'),

    menuNews: utils.getId('menuNews'),
    menuNotice: utils.getId('menuNotice'),
    menuSetting: utils.getId('menuSetting'),

    appVersion: utils.getId('appVersion'),
    notifUpdate: utils.getId('notifUpdate')
};

Elements.btnClose.addEventListener('click', () => {
    BrowserWindow.getFocusedWindow().close()
})

Elements.headClose.addEventListener('click', () => {
    BrowserWindow.getFocusedWindow().close()
})

Elements.minimize.addEventListener('click', () => {
    BrowserWindow.getFocusedWindow().minimize();
})

Elements.maximize.addEventListener('click', () => {
    BrowserWindow.getFocusedWindow().maximize();
})

// Appel Util
setInterval(function () {
    utils.verifMajBlog();
}, 900000);
utils.verifMaj(process.env.VERSION)

// Chargement des Menus

Elements.menuNews.addEventListener('click', () => {
    let modal = $("#modalChangelog");
    modal.modal('show')
    modal.find('.modal-title').html('Nouveauté')
    $.ajax({
        url: path.join(__dirname, 'modules/news.html'),
        success: (data) => {
            modal.find('.modal-body').html(data)
        },
        error: err => {
            notify.errorRestart(err)
        }
    })
})

Elements.menuNotice.addEventListener('click', () => {
    let modal = $("#modalNotice")
    modal.modal('show')
})

// Vérification de la configuration
if(!store.get('trainz_version')) {
    $("#warningState").html('<i class="fa fa-warning text-warning fa-lg" data-toggle="tooltip" title="Une ou plusieurs erreurs de configuration peuvent nuire au fonctionnement du launcher !"></i>')
}
if(!store.get('trainz_path')) {
    $("#warningState").html('<i class="fa fa-warning text-warning fa-lg" data-toggle="tooltip" title="Une ou plusieurs erreurs de configuration peuvent nuire au fonctionnement du launcher !"></i>')
}

// Affichage de la version du programme
Elements.appVersion.innerHTML = app.getVersion()

// Vérification des mise à jours
ipc.on('update_available', () => {
    ipc.removeAllListeners('update_available')
    Elements.notifUpdate.innerHTML = `<i class="icon-update text-success" data-toggle="tooltip" title="Une Mise à jour est disponible, Téléchargement en cours..."></i>`
})

ipc.on('update_downloaded', () => {
    ipc.removeAllListeners('update_downloaded')
    Elements.notifUpdate.innerHTML = `<i class="icon-upgrade text-success" data-toggle="tooltip" title="La mise à jour à été télécharger, veuillez redémarrer le launcher"></i>`
})



