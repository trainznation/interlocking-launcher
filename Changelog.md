# Tache à effectuer avant de build
- Regenerer les fichier de style
- Modifier la version du packages.json
- Mettre à jour à la date le changelog
- Definir debug à False
